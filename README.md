# cyber-live-m


Creates the Cybernux Live ISO utilizing the MATE Desktop.

Based on the configuration files for [Salix Live](https://github.com/djemos/slackware-salix-live).


## To build the live iso

You need to be root

```
sudo su
```

Install the slackware-live tool, which is used to create the initrd and build the iso

```
git clone https://gitlab.com/Cybernux/new-build-2021/slackware-live-for-cybernux.git
```

...then build and install the pkg

```
./maketxz.sh 
```

We also need SLI (salix live installer). Ours is modded to fit our needs.

```
git clone https://gitlab.com/Cybernux/new-build-2021/c-sli.git
```

...then build and install the pkg

```
./maketxz.sh 
```





Prepare the sources

```
cd cyber-live-m
sh prepare-sources.sh mate 
```


Then cd 64 or cd 32, depending on the arch you're building

```
cd 64
```

Get the prepared packages. Use 32|64 version e.g. 64 14.1

```
./getpkgs.sh 64 14.1
```

Build the iso

```
./build-iso.sh
```

You will find the iso in the 64 or 32 bit folder.


